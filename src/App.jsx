import React, {Component} from 'react';
import styled, {createGlobalStyle} from 'styled-components';
import Button from './components/Button';
import Modal from './components/Modal';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Container from './components/Container/Container';
import ItemsList from './components/ItemsList/ItemsList';

const GlobalStyle = createGlobalStyle`
  html, body {
    height: 100%;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  #root {
    display: flex;
    flex-direction: column;
    flex: 1 1 auto;
    height: 100%;
  }
`

const PageWrapper = styled.div`
width: 100%;
height: 100%;
margin: 64px auto;
padding: 20px 0;
`



class App extends Component {
    state = {
      isModalOpen: false,
      goodsCollection: [],
      cart: [],
      tryToCart: {},
      favorite: []

    }
    
    openModal = () => {
      this.setState({isModalOpen: true}, this.modalShow);
    }

    modalShow = () => document.querySelector('dialog').showModal();

    closeModal = () => {
      this.setState({isModalOpen: false});
    }
    
    confirmPress = () => {
      let newCart = this.state.cart;
      let item = this.state.tryToCart;
      newCart.push(item);
      this.setState({cart: newCart}, this.cartConsole);
      localStorage.setItem('cart', JSON.stringify(newCart))
      this.closeModal()
    }

    addToCart = (event) => {
      let article = +event.target.closest('.card').dataset.article;
      let item = this.state.goodsCollection.filter(item => item.article === article)[0];
      this.setState({tryToCart: item})
      this.openModal()
    }

    addToFavorite = (event) => {
      let article = +event.target.closest('.card').dataset.article;
      let newGoodsCollection = this.state.goodsCollection;
      newGoodsCollection.map(item => {
        if(item.article === article) {
          if(!item.isInFavorite) {
            item.isInFavorite = true;
          } else item.isInFavorite = false;
        }
      });
      let newFavorite = newGoodsCollection.filter(item => item.isInFavorite);
      this.setState({favorite: newFavorite});
      this.setState({goodsCollection: newGoodsCollection});
      localStorage.setItem('favorite', JSON.stringify(newFavorite));
    }

        
    sendRequest = () => {
      fetch(`${document.location.href}externalGoods.JSON`)
      .then((response) => response.json())
      .then((data) => this.setState({goodsCollection: data}, this.makeFavorite));
      
    }

    getCart = () => {
      let cart = JSON.parse(localStorage.getItem('cart'));
      if(cart) {
        this.setState({cart});
      }
    }

    getFavorite = () => {
      let favorite = JSON.parse(localStorage.getItem('favorite'));
      if(favorite) {
        this.setState({favorite});
      }
    }

    makeFavorite = () => {
      let allgoodsCollection = this.state.goodsCollection;
      let favorite = this.state.favorite;
      
      allgoodsCollection.forEach(item => {
        favorite.forEach(fav => {
          if(item.article === fav.article) {
            item.isInFavorite = true;
          }
        })
      })
      this.setState({goodsCollection: allgoodsCollection})
      
    }

    componentDidMount() {
      this.sendRequest();
      this.getCart();
      this.getFavorite();
    }

    render() {
        
        return (
          <>
            <GlobalStyle/>
            <Header cartAmount={this.state.cart.length} favoriteAmount={this.state.favorite.length}/>
            <PageWrapper>
              <Container>
                <ItemsList itemsList={this.state.goodsCollection} openModal={this.addToCart} addToFavorite={this.addToFavorite}/>
              </Container>
            </PageWrapper>
            {this.state.isModalOpen && (<Modal closeButton={true} actions={
              <>
                <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={this.confirmPress}>Yes</Button>
                <Button backgroundColor='rgba(0, 0, 0, 0.2)' onClick={this.closeModal}>Cancel</Button>
              </>
            } closeClick={this.closeModal} backgroundColor='#aeb5b8' header='Are you sure?' text="Do you really want to add this item to cart?">
              
            </Modal>)}
            
            <Footer/>
          </>
        )
    }
}

export default App;
