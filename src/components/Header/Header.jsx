import {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Button from '../Button';
import Container from '../Container/Container';


const StyledHeader = styled.header`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(3,37,65, 1);
    min-height: 64px;
    width: 100%;
    transition: top 0.2s linear;
    position: fixed;
    top: 0;
    
`

const FlexSpaceWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 20px;
`
const CounterWrapper = styled.div`
    position: relative;
`
const Counter = styled.span`
    position: absolute;
    top: -2px;
    right: -10px;
    font-size: 16px;
    color: orange;
`

export default class Header extends Component {
	render() {
        const {cartAmount, favoriteAmount} = this.props;
		return (
			<StyledHeader>
				<Container>
					<FlexSpaceWrapper>
						<div className="header__logo">
							<a href="/" className="logo">
								<svg xmlns="http://www.w3.org/2000/svg" width="100" viewBox="0 0 216 106.704">
                                    <g fillRule="evenodd" clipRule="evenodd">
                                        <path fill="#fff22d" d="M0 52.848h216v53.856H0V52.848z" />
                                        <path fill="#1376b8" d="M0 0h216v52.776H0V0z" />
                                    </g>
                                </svg>
							</a>
						</div>
                        <FlexSpaceWrapper>
                            <CounterWrapper className='favorite'>
                                <Button width='auto' height='auto'>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 32 32" fill="white">
                                        <path fill="#fff"
                                            d="M16,23.950931 L23.3243448,27.8015669 L21.9255194,19.6457891 L27.8510389,13.8698352 L19.6621724,12.6799232 L16,5.25955146 L12.3378276,12.6799232 L4.14896114,13.8698352 L10.0744806,19.6457891 L8.67565518,27.8015669 L16,23.950931 Z M9.60633744,29.5718298 C8.62864827,30.0858314 7.41939395,29.7099384 6.90539234,28.7322492 C6.7007142,28.3429282 6.63008485,27.8969921 6.70443854,27.4634768 L7.92551943,20.3440289 L2.75293776,15.3020079 C1.9619706,14.5310055 1.94578599,13.264779 2.71678842,12.4738118 C3.02380562,12.1588445 3.42609045,11.9538702 3.86136451,11.8906212 L11.0096966,10.8519077 L14.2065278,4.37442004 C14.6953724,3.38391272 15.8946241,2.97723472 16.8851314,3.46607931 C17.2795566,3.66073979 17.5988117,3.97999483 17.7934722,4.37442004 L20.9903034,10.8519077 L28.1386355,11.8906212 C29.2317252,12.0494564 29.9890881,13.0643421 29.8302529,14.1574318 C29.7670039,14.5927059 29.5620295,14.9949907 29.2470622,15.3020079 L24.0744806,20.3440289 L25.2955615,27.4634768 C25.4822835,28.5521497 24.7511078,29.5860616 23.6624349,29.7727836 C23.2289197,29.8471373 22.7829835,29.7765079 22.3936626,29.5718298 L16,26.2104824 L9.60633744,29.5718298 Z" />
                                    </svg>
                                    <Counter>{favoriteAmount}</Counter>
                                </Button>
                            </CounterWrapper>
                            
                            <CounterWrapper className='cart'>
                                <Button width='auto' height='auto'>
                                    <svg xmlns="http://www.w3.org/2000/svg" data-name="Layer 2" width="32px" height="32px" viewBox="0 0 35 35" fill="#fff">
                                        <path
                                            d="M27.47,23.93H14.92A5.09,5.09,0,0,1,10,20L8,11.87a5.11,5.11,0,0,1,5-6.32h16.5a5.11,5.11,0,0,1,5,6.32l-2,8.15A5.1,5.1,0,0,1,27.47,23.93ZM12.94,8.05a2.62,2.62,0,0,0-2.54,3.23l2,8.15a2.6,2.6,0,0,0,2.54,2H27.47a2.6,2.6,0,0,0,2.54-2l2-8.15a2.61,2.61,0,0,0-2.54-3.23Z" />
                                        <path
                                            d="M9.46 14a1.25 1.25 0 0 1-1.21-1L6.46 5.23A3.21 3.21 0 0 0 3.32 2.75H1.69a1.25 1.25 0 0 1 0-2.5H3.32A5.71 5.71 0 0 1 8.9 4.66l1.78 7.77a1.24 1.24 0 0 1-.93 1.5A1.43 1.43 0 0 1 9.46 14zM15.11 34.75a4 4 0 1 1 4-4A4 4 0 0 1 15.11 34.75zm0-5.54a1.52 1.52 0 1 0 1.52 1.52A1.52 1.52 0 0 0 15.11 29.21zM28.93 34.75a4 4 0 1 1 4-4A4 4 0 0 1 28.93 34.75zm0-5.54a1.52 1.52 0 1 0 1.53 1.52A1.52 1.52 0 0 0 28.93 29.21z" />
                                        <path
                                            d="M28.93,29.21H12.27a3.89,3.89,0,1,1,0-7.78h2.65a1.25,1.25,0,1,1,0,2.5H12.27a1.39,1.39,0,1,0,0,2.78H28.93a1.25,1.25,0,0,1,0,2.5Z" />
                                    </svg>
                                    <Counter>{cartAmount}</Counter>
                                </Button>
                            </CounterWrapper>
                            
                        </FlexSpaceWrapper>
					</FlexSpaceWrapper>
				</Container>
			</StyledHeader>
		)
	}
}

Header.propTypes = {
    cartAmount: PropTypes.number,
    favoriteAmount: PropTypes.number
}