import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Card from '../Card/Card';

const ItemWrapper = styled.div`
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    gap: 20px;
    padding-bottom: 45px; 
`



export default class ItemsList extends Component {

    render() {
        const {itemsList, openModal, addToFavorite} = this.props;

        const cardsRender = itemsList.map(({name, price, imgUrl, article, color, isInFavorite}) => {
            return (
                <Card data={article} key={article} isInFavorite={isInFavorite} name={name} price={price} imgUrl={imgUrl} article={article} color={color} openModal={openModal} addToFavorite={addToFavorite}/>
            )
        })
        return (
            <>
                <h2>Our goods</h2>
                <ItemWrapper>
                    {cardsRender}
                </ItemWrapper>
            </>
        )
    }
}

ItemsList.propTypes = {
    itemsList: PropTypes.array.isRequired,
    openModal: PropTypes.func,
    addToFavorite: PropTypes.func
}